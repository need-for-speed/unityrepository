### Race For Time ###
- Unity engine based racing game. 
- Players control a vehicle and race around a track in time trials or competitive multiplayer
- Playable on PC and mobile platforms
- Select tracks and cars for different races
- Mobile platforms utilize touch and tilt to steer the car


## Time Trials ##
- Finish laps as fast as you can, beat your high score

## Competitive Multiplayer ##
- Race against another player to see who's the better driver