﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	float speed = 35f;
	float torque = 150f;
	float driftFactorSticky = 0.5f;
	float driftFactorSlippy = 0.9f;
	float maxStickey = 2.5f;

    Timer timer;
    Text timeLabel;
    Text lapLabel;


	// Use this for initialization
	void Start () {
        timer = new Timer();
        timeLabel = GameObject.Find("timeLabel").GetComponent<Text>();
        lapLabel = GameObject.Find("lapLabel").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        
	}
    void FixedUpdate()
    {
		Rigidbody2D rb2d = GetComponent<Rigidbody2D> ();
		float driftFactor = driftFactorSticky;

		if (RightTurnSpeed ().magnitude > maxStickey) {
			driftFactor = driftFactorSlippy;
		}

		rb2d.velocity = ForwardSpeed () + RightTurnSpeed() * driftFactor;

		if (Input.GetButton ("Accelerate") || touchAccelerate() ) {
            speedUp();
		}

		if (Input.GetButton ("Brakes") || touchBrake() ) {
            slowDown();
		}
        float horizontal;
#if UNITY_STANDALONE || UNITY_WEBPLAYER
        horizontal = Input.GetAxis("Horizontal");
        updateTimers();
#elif UNITY_IOS || UNITY_ANDROID
        horizontal = (float) 
            1.5 * (-Input.acceleration.x);
        #endif
        float t = Mathf.Lerp(0, torque, rb2d.velocity.magnitude / 10);
        rb2d.angularVelocity = horizontal * t;
        updateTimers();
    }

    bool touchAccelerate()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];
            if(myTouch.position.x > Screen.width / 2) {
                return true;
            }
            return false;
        }else
        {
            return false;
        }
        
    }

    void updateTimers()
    {
        timeLabel.text = timer.getTime();
        lapLabel.text = timer.getLaps();
    }
    bool touchBrake()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];
            if (myTouch.position.x < Screen.width / 2)
            {
                return true;
            }
                return false;
        }
        else
        {
            return false;
        }
    }

	Vector2 ForwardSpeed(){
		return transform.up * Vector2.Dot (GetComponent<Rigidbody2D>().velocity, transform.up);
	}

	Vector2 RightTurnSpeed(){
		return transform.right * Vector2.Dot (GetComponent<Rigidbody2D>().velocity, transform.right);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        timer.crossedLine();
    }
    public void speedUp()
    {
        Rigidbody2D rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddForce(transform.up * speed);
    }
    public void slowDown()
    {
        Rigidbody2D rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddForce(transform.up * -speed / 2);
    }
    public void shout()
    {
        Debug.Log("WORKING!!!");
    }
}

class Timer
{
    private float time;
    private ArrayList lapTimes;
    private bool started;

    public Timer()
    {
        time = 0;
        started = false;
        lapTimes = new ArrayList();
    }

    public void crossedLine()
    {
        if(started)
        {
            lapTimes.Add(time);
            time = 0;
        } else
        {
            started = true;
        }
    }

    private string formatToString(float _time)
    {
        var minutes = Mathf.Floor(_time / 60f);
        var seconds = Mathf.Floor(_time - minutes * 60f);
        var milliseconds = _time - Mathf.Floor(_time);

        milliseconds = Mathf.Floor(milliseconds * 1000f);
        
        var sMinutes = "00" + minutes.ToString();
        sMinutes = sMinutes.Substring(sMinutes.Length - 2);

        var sSeconds = "00" + seconds.ToString();
        sSeconds = sSeconds.Substring(sSeconds.Length - 2);

        var sMilliseconds = "000" + milliseconds.ToString();
        sMilliseconds = sMilliseconds.Substring(sMilliseconds.Length - 3);

        string timeText = sMinutes + ":" + sSeconds + ":" + sMilliseconds;

        return timeText;
    }

    public string getTime()
    {
        // call this on every update and increment it by one every time
        if(started) time = time + 1;
        return formatToString(time);
    }

    public string getLaps()
    {
        string ret = "Laps\n";

        foreach (float lapTime in lapTimes)
        {
            ret += formatToString(lapTime) + '\n';
        }

        return ret;
    }
    
}
