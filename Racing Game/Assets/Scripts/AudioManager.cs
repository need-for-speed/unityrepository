﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume = 0.7f;
    [Range(0f, 1f)]
    public float pitch = 1f;

    AudioSource source;

    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
    }

    public void Play()
    {
        source.volume = volume;
        source.pitch = pitch;
        source.Play();
    }

}

public class AudioManager : MonoBehaviour
{

    public AudioManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Instance error");
        }
        else
        {
            instance = this;
        }
    }

    [SerializeField]
    Sound[] sounds;
    public AudioSource source;

    void Start()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + sounds[i].name);
            sounds[i].SetSource(_go.AddComponent<AudioSource>());
        }
        PlaySound("Track Music");
    }

    bool touchAccelerate()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];
            if (myTouch.position.x > Screen.width / 2)
            {
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }

    }

    bool touchBrake()
    {
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];
            if (myTouch.position.x < Screen.width / 2)
            {
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    void Update()
    {
        if (Input.GetButton("Accelerate") || touchAccelerate())
        {
            PlaySound("Accelerate");
        }

        if (Input.GetButton("Brakes") || touchBrake())
        {
            PlaySound("Brakes");
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider == true)
        {
            PlaySound("Collision Sound");
        }
    }


    public void PlaySound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                sounds[i].Play();
                return;
            }
        }

       // Debug.LogWarning("No sound with that name exisits");

    }

}