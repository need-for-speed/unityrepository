﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public static Transform target;	//target for the camera to follow
	public float xOffset;			//how much x-axis space should be between the camera and target
    public Quaternion rotation = Quaternion.identity;
    public float dampTime = .15f;
    private Vector3 velocity;
    public float rotationDamping = 10.0f;

	void Update()
	{
        transform.position = new Vector3(target.position.x , target.position.y, -10f);
            
        transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * rotationDamping);
	}
}
